//
//  ViewController.m
//  AudioNeo
//
//  Created by Jumpei Katayama on 2/27/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import "ViewController.h"


@interface ViewController (){
}


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    player[0] = [self makeAudioPlayer:@"250Hz_44100Hz"];
    player[1] = [self makeAudioPlayer:@"440Hz_44100Hz"];
    
}

-(AVAudioPlayer*)makeAudioPlayer:(NSString*)fileName{
    NSString* path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    NSLog(@"%@", path);
    NSURL* url = [NSURL fileURLWithPath:path];
    
    NSError *error = [[NSError alloc] init];
    return [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
}
- (IBAction)start:(id)sender {
    if (player[0].playing) {
        NSLog(@"STOP");
        [player[0] stop];
    }else {
        NSLog(@"playing!");
        [player[0] play];
        player[0].numberOfLoops = 999;
        player[0].currentTime = 0;
    }
}
- (IBAction)startSE:(id)sender {
    if (player[1].playing) {
        player[1].currentTime = 0;
    }else {
        [player[1] play];
    }
}

- (IBAction)VolumeUp:(id)sender {
    float volume = player[0].volume+0.2f;
    if (volume > 1.0) {
        volume = 1.0f;
    }
    NSLog(@"%f", volume);
    player[0].volume = volume;
    player[1].volume = volume;
}

- (IBAction)VolumeDown:(id)sender {
    float volume = player[0].volume-0.2f;
    if (volume < 0.0f) {
        volume = 0.0f;
    }
    NSLog(@"%f", volume);
    player[0].volume = volume;
    player[1].volume = volume;
}

- (void)TouchButton {
    
}


@end
