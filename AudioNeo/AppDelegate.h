//
//  AppDelegate.h
//  AudioNeo
//
//  Created by Jumpei Katayama on 2/27/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

